﻿using System;
using System.IO;
using System.Linq;

namespace AMRReadingReprocessor.Common
{
    public static class Enumerations
    {
        public enum EnPopupTypes
        {
            Toast,
            Dialog
        };

        public enum EnMessageTypes
        {
            Info,
            Warn,
            Error
        };

        public enum EnHostService
        {
            idp,
            exceptionMeterReading,
            expLocalIP
        };

        public enum EnHostCommunicationResult
        {
            success,
            userCancelled,
            communicationError,
            invalidCredentials,
        };

        public enum EnHostEnvironment
        {
            Prod = 0,
            PreProd,            
            QA,
            Dev,
            Ca,
            CustomIp
        };

        public enum ServiceType
        {
            LocalIP = 0,
            IPD,
            Identity,
            RouteManagement,
            Utility,
            NeptuneConnect,
            FieldManagement
        };

        public enum EnInetConnectionStatus
        { 
            Disconnected,
            Connected
        }

        public enum EnCountry
        {
            USA = 1,
            Canada
        }

        //Custom Additions Below Here

        public enum ProgramRegisterConnectivityDialogResult
        {
            Cancel = 0,
            Coil,
            TwoWire,
            ThreeWire

        };

        public enum ActionSheetType
        {
            None = 0,
            MouseConnect,
            BluetoothScan,
            Formats,
            Programming,
            WireType
        };

        public enum ActionSheetButtonId
        {
            None = 0,
            Button1,
            Button2,
            Button3,
            Button4,
            Button5
        };

        public enum DialogResultOption
        {
            Ok = 1,
            Cancel,
            Yes,
            No
        };

    }

    public static class Strings
    {
        public class N360Domains
        {
            public class USA
            {
                public const string Prod = "https://www.neptune360.com/";
                public const string PreProd = "https://www.pre-prod.neptune360.com/";
                public const string Qa = "http://www.qa.neptune360.com/";
                public const string Dev = "http://www.dev.neptune360.com/";
            }

            public class Canada
            {
                public const string Prod = "https://canada.neptune360.com/";
            }
        }

        public const string RemoteHostTestUrl = "https://www.google.com/";
        public const string DemoUserName = "demouser2020" ;
        public const string DemoPassword = "!2Neptune$20$0";

        public const string IDPClentScope = "identityService routeManagementService utilityService";

        /// <summary>
        /// Client ID is mobilefieldapplications
        /// </summary>
        /// <returns></returns>
        public static string IDPClientID
        {
            get
            {
                //return "smartclient";
                //return "mobilefieldapplications";
                byte[] value = new byte[] { 0x4e, 0x65, 0x70, 0x74, 0x75, 0x6e, 0x65, 0x33, 0x36, 0x30, 0x4d, 0x6f, 0x62, 0x69, 0x6c, 0x65 };
                string ret = "";
                value.ToList<byte>().ForEach(a => ret += (char)a);
                return ret;
            }
        }

        /// <summary>
        /// client secret is N3ptun3360M0bileFIeldApplicati0n!@#$
        /// </summary>
        /// <returns></returns>
        public static string IDPClientSecret
        {
            get
            {
                //return "N3ptun3360SmartClient1l3!@#$";
                //return "N3ptun3360M0bileFIeldApplicati0n!@#$";
                byte[] value = new byte[] { 0x4e, 0x33, 0x70, 0x74, 0x75, 0x6e, 0x33, 0x33, 0x36, 0x30, 0x4d, 0x30, 0x62, 0x31, 0x6c, 0x33, 0x21, 0x40, 0x23, 0x24 };
                string ret = "";
                value.ToList<byte>().ForEach(a => ret += (char)a);
                return ret;
            }
        }


        public static string CreateUserName = "MobileOnlyUser";
        public static string CreateUserPassword = "N3ptun3 WinyourDAY Mouse!";

        public const string LAST_DIGIT_RESOLUTION_FULLY_ENCODED = "9";
        public const string LAST_DIGIT_RESOLUTION_PARTIALLY_ENCODED = "5";

        public const string OUTPUT_MODE_2W = "2W";
        public const string OUTPUT_MODE_3W = "3W";

        public const string PROCODER_OUTPUT_STANDARD = "0";
        public const string PROCODER_OUTPUT_PRO_READ = "1";

        public const string NETWORK_TYPE_SINGLE = "Single";
        public const string NETWORK_TYPE_NETWORK = "Network";

        public const string SUPPORT_PHONE = "800-647-4832";
        public const string SUPPORT_EMAIL = "support@neptunetg.com";

        public const string MOBILE_APPLICATION_USER = "Mobile Application User";
    }

    public class Folders
    {
        public const string Logs = "logs";
        public const string Transactions = "transactions";
        public const string Data = "data";
        public const string Formats = "formats";
        public const string UserFormats = "userformats";
    }

    public class FileNames
    {
        // Common For All Apps
        public const string AppLogFile = "app_logs_{0}.txt";
        public const string NFPUserFormat = "nfp_user_format_{0}.json";
        public const string NFPPresetFormats = "nfp_preset_formats.json";
        public const string NFPTransaction = "nfp_trans_{0}.json";
    }

    public class FileSpec
    {
        public const string NFPUserDefinedFormat = "nfp_user_format*.json";
        public const string NFPLogs = "app_logs*.txt";
        public const string NFPTransactions = "nfp_tran*.json";
    }

    public class PropNames
    {
        public class UserPref
        {
            public const string FirstName = "FirstName";
            public const string LastName = "LastName";
            public const string Email = "Email";
            public const string Password = "Password";
            public const string AuthToken = "AuthToken";
            public const string RefreshToken = "RefreshToken";
            public const string HostEnvironment = "HostEnvironment";
        }

        public class Globals
        {
            public const string HasInternetConnection = "HasInternetConnection";
            public const string RemoteEnvType = "RemoteEnvType";
            public const string IsCanadaUser = "IsCanadaUser";
            public const string IsDevMode = "IsDevMode";
            public const string ShouldNotifiyToast = "ShouldNotifiyToast";
            public const string ShouldNotifiySimpleDialog = "ShouldNotifiySimpleDialog";
            public const string ShouldWait = "ShouldWait";
            public const string ShouldNotifiyPromptDialog = "ShouldNotifiyPromptDialog";
            public const string ShouldNotifiySimpleDialogNoTranslate = "ShouldNotifiySimpleDialogNoTranslate";
            public const string ShouldSendDataToHost = "ShouldSendDataToHost";
            public const string LastErrorMessage = "LastErrorMessage";
            public const string ShouldLogOff = "ShouldLogOff";
            public const string ShouldLogOn = "ShouldLogOn";
            public const string IsLoggedOn = "IsLoggedOn";
            public const string ShouldInitializeApp = "ShouldInitializeApp";
            public const string IsMouseConnected = "IsMouseConnected";
            public const string FPActionSheetType = "FPActionSheetType";
        }
    }

    public class MessageCenterKeys
    {
        public const string FormatFavoriteChange = "{9B6B703D-242C-48DB-8C9D-D447FE15EEE9}";
        public const string FPMainMenuStartChange = "{CDE39783-DB95-4E50-ADA6-130620BB66B1}";
        public const string CASButtonClicked = "{1FBC9941-A45B-44E4-B7D0-C9BAB79B64D9}";
        public const string AddNewFormat = "{85780086-C318-448D-A3CD-31F7F0CF6375}";
        public const string DeleteCustomFormat = "{5E725500-C4D7-4059-9D60-4D68E937BAC4}";
        public const string BluetoothConnected = "{C63D2C7B-F346-41C5-888E-1B11BA7C3551}";
        public const string BluetoothDisconnected = "{8E60CBF7-F0B7-4B6A-BB45-72A255AF5FD7}";
    }

    public class AppPropertyKeys
    {
        public const string ToastMessageKey = "{955ED21D-3BEE-42EF-B019-3920F2A958EC}";
        public const string DialogMessageKey = "{1EA0F552-6E17-4AE6-AE15-C2B295029BB6}";
    }

    public class SharedServices
    {
        public static class MMRService
        {
            public const string AvailableRa = "routemanagement/api/mmr/availablera/{0}";
            public const string RequestRouteAssignment = "routemanagement/api/mmr/requestassignment/{0}/{1}";
            public const string RequestRouteUnassignment = "routemanagement/api/mmr/requestunassignment/{0}";
            public const string RequestAssignedOrders = "routemanagement/api/mmr/assignedorders/{0}";
            public const string SetStatusByRoute = "routemanagement/api/mmr/setstatusbyroute/{0}";
            public const string SetStatusByAss = "routemanagement/api/mmr/setstatusbyassignment/{0}";
            public const string UploadReadings = "routemanagement/api/mmr/uploadreadings/{0}";
            public const string SiteCodes = "routemanagement/api/mmr/sitecodes/{0}";
            public const string UserUtilities = "routemanagement/api/mmr/getbyuserid/{0}";
            public const string UploadBreadcrumb = "routemanagement/api/mmr/uploadbreadcrumb/{0}";
            public const string GetUtilityBySiteId = "routemanagement/api/mmr/getutilitybysiteid/{0}";
            public const string GetRoutesByUtilityRole = "routemanagement/api/mmr/getroutesbyutilityrole/{0}/{1}";
        }

        public class NeptuneConnectService
        {
            public static class NeptuneConnect
            {
                public const string GetUtilitiesByUserId = "neptuneconnect/api/NeptuneConnect/GetUtilitiesByUserId/{0}";
                public const string GetAppVersions = "neptuneconnect/api/NeptuneConnect/GetSmartClientAppVersions";
                public const string GetTrimbleCab = "neptuneconnect/api/NeptuneConnect/DownloadTrimbleCab/{0}";
            }
        }

        public class FieldProgrammerService
        {
            public const string UploadDataFileV1 = "mobilefieldmanagement/api/v1/fieldprogrammer/uploaddatafile/{0}/{1}";
            public const string PresetFormatsV1  = "mobilefieldmanagement/api/v1/fieldprogrammer/getpresetformats/{0}";
        }

        public class IdentityService {
            public const string CreateMobileApplicationUser = "identity/api/users/apps/createmobileappuser";
            public const string ForgotPasswordReset = "identity/api/users/sendforgotpasswordresetbyemail";
            public const string GetRole = "identity/api/roles/{0}";
        }

        public class UtilityService {
            public const string GetUtilities = "utility/api/v2/utilities/getbyuserid/{0}";
        }

        public class ImportExportService {
        }

        public class RouteManagementService {
        }

        public class NotificationService {
        }

        public class ConfigMgrService {
        }

        public class ShadowService {
        }

        public class AlertsService {
        }

        public class DataWarehouseService {
        }

        public class FileMapperServcie {
        }

        public class ApidDploymentService {
        }

        public class DeviceManagementService {
        }

        

    }

    public struct Constants
    {
        public static int DELAY_BETWEEN_CONSECUTIVE_COMMANDS = 500;
    }
}


