﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Neptune.MFM.Common;
using Neptune.MFM.Models;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Essentials;
using Neptune.MFM.Services;
using Neptune.MFM.Interfaces;
using System.Threading;
using __Enum =  Neptune.MFM.Common.Enumerations;
using NFPCommunication;
using Neptune.MFM.Controls;

namespace Neptune.MFM.Common
{
    public static class EnumUtil
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }

    public static class Helpers
    {
        public static class Urls
        {
            public static string GetHostProtocol(Enumerations.EnHostEnvironment envType)
            {
                switch (envType)
                {
                    case Enumerations.EnHostEnvironment.Ca:
                    case Enumerations.EnHostEnvironment.PreProd:
                    case Enumerations.EnHostEnvironment.Prod:
                        return "https";
                    default:
                        return "http";
                }
            }

            public static string GetIdpUrl(Enumerations.EnCountry enCountry, Enumerations.EnHostEnvironment envType)
            {
                string lEnvPart = "";
                switch (envType)
                {
                    case Enumerations.EnHostEnvironment.Dev:
                        lEnvPart = "dev.";
                        break;
                    case Enumerations.EnHostEnvironment.QA:
                        lEnvPart = "qa.";
                        break;
                    case Enumerations.EnHostEnvironment.PreProd:
                        lEnvPart = "pre-prod.";
                        break;
                    default:
                        lEnvPart = "";
                        break;
                }

                return enCountry == Enumerations.EnCountry.USA ?
                        string.Format(string.Format($"{GetHostProtocol(envType)}://idp.{lEnvPart}neptune360.com")) :
                        string.Format(string.Format($"{GetHostProtocol(envType)}://ca-idp.{lEnvPart}neptune360.com"));
            }
        }

        public static void RunAsync(Task task)
        {
            task.ContinueWith(t =>
            {
                ILogger logger = DependencyService.Get<ILogManager>().GetLog();
                logger.Error("Unexpected Error", t.Exception);

            }, TaskContinuationOptions.OnlyOnFaulted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logFilePath"></param>
        /// <param name="ts"></param>
        /// <returns></returns>
        public static async Task<bool> CleanupLogs(string logFilePath, TimeSpan ts)
        {
            bool retVal = true;
            ILogger logger = DependencyService.Get<ILogManager>().GetLog();

            try
            {

                DateTime today = DateTime.Now;

                if (!Directory.Exists(logFilePath)) {
                    throw new Exception($"Path [{logFilePath}] does not exist!");
                }

                var files = Directory.GetFiles(logFilePath, "*.txt");
                if (files != null)
                {
                    List<string> lFiles = files.ToList<string>();
                    foreach (var file in lFiles)
                    {
                        var fileDate = File.GetCreationTime(file);
                        TimeSpan days = today - fileDate;
                        if (days > ts)
                        {
                            try {
                                File.Delete(file);
                            } 
                            catch(Exception _ex) {
                                logger.Warn(_ex.Message, _ex);
                            }
                        }
                    }
                }
            }
            catch (Exception _ex)
            {
                logger.Error(_ex.Message, _ex);
                throw _ex;
            }
            return await Task.FromResult(retVal);
        }

        //public static async Task<bool> CopySomeFilesForTesting()
        //{
        //    bool retVal = true;
        //    ILogger logger = DependencyService.Get<ILogManager>().GetLog();
        //    try
        //    {

        //        for (int i = 1; i < 11; i++) {
        //            DateTime dt = DateTime.Now.AddDays(-i);

        //            string fName = string.Format(Common.FileNames.NFPReads, dt.ToString("yyyyMMdd"));
        //            string copyToFile = Path.Combine(FileService.QueryReadsFolder, fName);

        //            try
        //            {
        //                File.Copy(FileService.ReadsFullFilePath, copyToFile);
        //            }
        //            catch (Exception ex)
        //            {
        //                logger.Warn(ex.TraceSummary());
        //            }
        //        }

        //    }
        //    catch (Exception _ex)
        //    {
        //        logger.Error(_ex.TraceSummary());
        //    }
        //    return await Task.FromResult(retVal);
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static async Task<bool> SendTransactionFilesAsync()
        {
            bool retVal = true;
            var logger = DependencyService.Get<ILogManager>().GetLog();
            var apiService = DependencyService.Resolve<ApiService>();

            try
            {
                if (!Directory.Exists(FileService.TransactionsFolder))
                    return false;

                var aFiles = Directory.GetFiles(FileService.TransactionsFolder, Common.FileSpec.NFPTransactions);
                if (aFiles == null || aFiles.Length == 0)
                    return true;

                List<string> lFiles = aFiles.ToList();
                if (lFiles?.Count > 0)
                {
                    using (var cancelToken = new CancellationTokenSource())
                    {
                        foreach (var file in lFiles)
                        {
                            logger.Debug(file);

                            if (Plugin.Connectivity.CrossConnectivity.Current.IsConnected == false)
                            {
                                cancelToken.Cancel();
                            }

                            SendFileModel sfm = new SendFileModel
                            {

                                FileName = Path.GetFileName(file),
                                FullPath = file
                            };

                            var result = await apiService.SendTransactionFile(sfm, cancelToken.Token);
                            //var result = await Task.Delay(500).ContinueWith( _=> { return true;  });
                            if (result)
                            {
                                try
                                {
                                    logger.Debug($"Deleting file: {sfm.FileName}");
                                    System.IO.File.Delete(sfm.FullPath);
                                }
                                catch (Exception _ex)
                                {
                                    logger.Error(_ex.TraceSummary());
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception _ex)
            {
                logger.Error(_ex.TraceSummary());
            }

            return await Task.FromResult(retVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static async Task<bool> SendLogsFilesAsync()
        {
            bool retVal = true;

            var logger = DependencyService.Get<ILogManager>().GetLog();
            var apiService = DependencyService.Resolve<ApiService>();

            try
            {
                var logFolder = FileService.LogFilesFolder;
                var logFiles = Directory.GetFiles(logFolder, FileSpec.NFPLogs)?.ToList();
                if (logFiles != null)
                {
                    var todaysFile = FileService.AppLogFullFilePath;

                    using (var cancelToken = new CancellationTokenSource())
                    {
                        foreach (var logFile in logFiles)
                        {

                            logger.Debug(logFile);

                            if (Plugin.Connectivity.CrossConnectivity.Current.IsConnected == false)
                            {
                                cancelToken.Cancel();
                            }

                            SendFileModel sfm = new SendFileModel
                            {
                                FileName = Path.GetFileName(logFile),
                                FullPath = logFile
                            };

                            var result = await apiService.SendLogFiles(sfm, cancelToken.Token);
                            if (result)
                            {
                                logger.Debug($"[logFile] Was Submitted Successfully");
                                try 
                                {
                                    if (logFile != todaysFile)
                                    {
                                        File.Delete(logFile);
                                    }
                                } 
                                catch (Exception _ex)                                 
                                {
                                    logger.Error(_ex.Message, _ex);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _ex)
            {
                logger.Error(_ex.Message, _ex);
            }

            return await Task.FromResult(retVal);
        }

        public static async Task<List<NFPFormat>> GetRemotePresetFormats(DateTime loadDate = default)
        {
            List<NFPFormat> retVal = new List<NFPFormat>();

            var logger = DependencyService.Get<ILogManager>().GetLog();
            var apiService = DependencyService.Resolve<ApiService>();

            try
            {
                using (var cancelToken = new CancellationTokenSource())
                {
                    retVal = await apiService.RetrievePresetFormats(loadDate, cancelToken.Token);
                }
            }
            catch (Exception _ex)
            {
                logger.Error(_ex.Message, _ex);
            }

            return retVal;
        }


        public static async Task<string> GetAppVersions()
        {
            string retVal = string.Empty;

            var logger = DependencyService.Get<ILogManager>().GetLog();
            var apiService = DependencyService.Resolve<ApiService>();

            try
            {
                using (var cancelToken = new CancellationTokenSource())
                {
                    retVal = await apiService.GetAppVersions(cancelToken.Token);
                }
            }
            catch (Exception _ex)
            {
                logger.Error(_ex.Message, _ex);
            }

            return retVal;
        }

        public static async Task<bool> CheckForNewData()
        {
            bool retVal = false;

            var logger = DependencyService.Get<ILogManager>().GetLog();
            //var pRService = DependencyService.Resolve<NFPReadMgtService>();
            var apiService = DependencyService.Resolve<ApiService>();

            try
            {
                var logFolder = FileService.LogFilesFolder;
                var logFiles = Directory.EnumerateFileSystemEntries(logFolder, "*.txt")?.ToList();
                if (logFiles != null)
                {
                    if (logFiles.Count > 1)
                    {
                        return await Task.FromResult(true);
                    }
                }

                //var pList = await pRService.ProcessReads(FileService.QueryReadsFolder, FileService.ReadsFullFilePath);
                //if (pList?.Count > 0)
                //{

                //}
            }
            catch (Exception _ex)
            {
                logger.Error(_ex.Message, _ex);
            }
            return await Task.FromResult(retVal);
        }

        public static async Task<GpsData> GetCurrentGPSCoordinates()
        {
            var logger = DependencyService.Get<ILogManager>().GetLog();

            GpsData retVal = new GpsData();

            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Best);
                var location = await Geolocation.GetLocationAsync(request);
                if (location != null)
                {
                    retVal.GpsLocation = location;
                    logger.Info($"Latitude: {location.Latitude}, Longitude: {location.Longitude}, Altitude: {location.Altitude}");
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                logger.Error(fnsEx.Message, fnsEx);
                retVal.GpsErrorMessage = fnsEx.Message;
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
                logger.Error(fneEx.Message, fneEx);
                retVal.GpsErrorMessage = fneEx.Message;
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                logger.Error(pEx.Message, pEx);
                retVal.GpsErrorMessage = pEx.Message;
            }
            catch (Exception ex)
            {
                // Unable to get location
                logger.Error(ex.Message, ex);
                retVal.GpsErrorMessage = ex.Message;
            }

            return await Task.FromResult(retVal);
        }


        //public static bool SendToast(string message, CDialogMessage.MessageTypes msgType = CDialogMessage.MessageTypes.Info)
        //{
        //    Task.Delay(500);
        //    var pDlg = new CDialogMessage
        //    {
        //        PopupType = CDialogMessage.PopupTypes.Toast,
        //        Message = message,
        //        MessageType = msgType
        //    };
        //    Application.Current.Properties[AppPropertyKeys.DialogMessageKey] = pDlg;
        //    DependencyService.Get<GlobalWatchList>().ShouldNotifiyToast = true;
        //    return true;
        //}

        public static async Task<bool> SendToastAsync(string message, __Enum.EnMessageTypes msgType = __Enum.EnMessageTypes.Info)
        {
            await Task.Delay(500);
            var pDlg = new CDialogMessage { 
                PopupType = __Enum.EnPopupTypes.Toast,
                Message = message,
                MessageType = msgType            
            };
            Application.Current.Properties[AppPropertyKeys.DialogMessageKey] = pDlg;
            DependencyService.Get<GlobalWatchList>().ShouldNotifiyToast = true;
            return await Task.FromResult(true);
        }

        //public static async Task<bool> SendToastAsync(CDialogMessage pMsg)
        //{
        //    await Task.Delay(500);
        //    Application.Current.Properties[AppPropertyKeys.DialogMessageKey] = pMsg;
        //    DependencyService.Get<GlobalWatchList>().ShouldNotifiyToast = true;
        //    return await Task.FromResult(true);
        //}

        public static async Task<bool> ShowSimpleDialogAsync(string title, string message, __Enum.EnMessageTypes msgType = __Enum.EnMessageTypes.Info)
        {
            var pDlg = new CDialogMessage
            {
                PopupType = __Enum.EnPopupTypes.Dialog,
                Title = title,
                Message = message,
                MessageType = msgType
            };
            Application.Current.Properties[AppPropertyKeys.DialogMessageKey] = pDlg;
            DependencyService.Get<GlobalWatchList>().ShouldNotifiySimpleDialog = true;
            return await Task.FromResult(true);
        }



        public static List<PickerItemNameValue> GetLastDigitResolutionList()
        {
            return new List<PickerItemNameValue> 
            {
                new PickerItemNameValue{ Name = "Fully encoded", Value = Strings.LAST_DIGIT_RESOLUTION_FULLY_ENCODED },
                new PickerItemNameValue{ Name = "Partially encoded", Value = Strings.LAST_DIGIT_RESOLUTION_PARTIALLY_ENCODED }
            };
        }


        public static List<PickerItemNameValue> GetOutputModeList()
        {
            return new List<PickerItemNameValue>
            {
                new PickerItemNameValue{ Name = "2-wire", Value = Strings.OUTPUT_MODE_2W },
                new PickerItemNameValue{ Name = "3-wire", Value = Strings.OUTPUT_MODE_3W }
            };
        }

        public static List<PickerItemNameValue> GetProCoderOutputList()
        {
            return new List<PickerItemNameValue>
            {
                new PickerItemNameValue{ Name = "Standard", Value = Strings.PROCODER_OUTPUT_STANDARD },
                new PickerItemNameValue{ Name = "ProRead only", Value = Strings.PROCODER_OUTPUT_PRO_READ }
            };
        }

        public static async Task<bool> TryConnectLastDevice()
        {
            bool retVal = false;

            var logger = DependencyService.Get<ILogManager>().GetLog();
            var nfpProcessor = DependencyService.Resolve<NFPProcessor>();

            var userPrefs = DependencyService.Get<Services.UserPreferenceService>();
            var pDevice = userPrefs.UserPreferences.LastBTDevice;
            if (pDevice != null && !string.IsNullOrEmpty(pDevice.DeviceID))
            {
                if (nfpProcessor != null)
                {
                    try
                    {
                        using (var act = new ActivityTimerDialog())
                        {
                            retVal = await nfpProcessor?.Connect(pDevice.DeviceID);
                        }
                        if(retVal)
                        {
                            return await Helpers.SendToastAsync($"Connected device {pDevice.Name}");
                        }
                    }
                    catch (Exception ex)
                    {

                        logger.Error(ex.Message, ex);
                    }
                }
            }
            return await Task.FromResult(retVal);
        }


        public static async Task<bool> SendCustomFormatFilesAsync()
        {
            bool retVal = true;

            var logger = DependencyService.Get<ILogManager>().GetLog();
            var apiService = DependencyService.Resolve<ApiService>();

            try
            {
                var folder = FileService.UserFormatFilesFolder;
                var files = Directory.GetFiles(folder, FileSpec.NFPUserDefinedFormat)?.ToList();
                if (files != null)
                {
                    using (var cancelToken = new CancellationTokenSource())
                    {
                        foreach (var file in files)
                        {

                            logger.Debug(file);

                            if (Plugin.Connectivity.CrossConnectivity.Current.IsConnected == false)
                            {
                                cancelToken.Cancel();
                            }

                            SendFileModel sfm = new SendFileModel
                            {
                                FileName = Path.GetFileName(file),
                                FullPath = file
                            };

                            var result = await apiService.SendUserFormatFiles(sfm, cancelToken.Token);
                            if (result)
                            {
                                logger.Debug($"{file} Was Submitted Successfully");

                                try
                                {
                                    File.Delete(file);
                                }
                                catch (Exception _ex)
                                {
                                    logger.Error(_ex.Message, _ex);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _ex)
            {
                logger.Error(_ex.Message, _ex);
            }

            return await Task.FromResult(retVal);
        }



        public static async Task<string> GetRoleName(string roleId)
        {
            string result = String.Empty;
            ILogger logger = DependencyService.Get<ILogManager>().GetLog();
            ApiService apiService = DependencyService.Resolve<ApiService>();

            using (var cancelToken = new CancellationTokenSource())
            {
                RoleResponse roleResponce = await apiService.GetRole(roleId, cancelToken.Token);
                if (roleResponce != null)
                {
                    return roleResponce.RoleName;
                }
            }
                return result;
        }


        public static async Task<List<UtilityAndCodeTypesResponse>> GetUtilities(string userId)
        {
            ILogger logger = DependencyService.Get<ILogManager>().GetLog();
            ApiService apiService = DependencyService.Resolve<ApiService>();

            using (var cancelToken = new CancellationTokenSource())
            {
                return await apiService.GetUtilities(userId, cancelToken.Token);
            }

        }
    }
}
