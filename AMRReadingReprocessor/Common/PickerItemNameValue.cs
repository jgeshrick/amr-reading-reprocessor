﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Neptune.MFM.Common
{
    public class PickerItemNameValue
    {
        public string Value { get; set; }
        public string Name { get; set; }
    }
}
