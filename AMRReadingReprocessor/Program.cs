﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AMRReadingReprocessor.Models;
using Newtonsoft.Json;
using __AuthService = AMRReadingReprocessor.Services.AuthService;

namespace AMRReadingReprocessor
{
    class Program
    {
        static string URI = "http://10.8.10.205:5011/";
        static string API = "api/mmr/uploadreadings/v2/{0}";
        static string SITEID = "01054";
        static string IDP = "https://ca-idp.neptune360.com";
        static string watchLocation = "/Users/jgeshrick/Public/Drop Box/amrreadings";
        static bool IsBackFill = false;

        static string user = "";
        static string pass = "";

        readonly object fileLock = new object();

        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    string arg = args[i];
                    switch (arg)
                    {
                        case "-?":
                            Console.WriteLine("Help");
                            return;
                        case "-uri":
                            URI = args[i + 1];
                            break;
                        case "-siteid":
                            SITEID = args[i + 1];
                            break;
                        case "-idp":
                            IDP = args[i + 1];
                            break;
                        case "-user":
                            user = args[i + 1];
                            break;
                        case "-pass":
                            pass = args[i + 1];
                            break;
                        case "-watch":
                            watchLocation = args[i + 1];
                            break;
                        case "-backfill":
                            IsBackFill = true;
                            break;

                    }
                }

                
            }
            else
            {
                Console.WriteLine("Need some args");
                Console.ReadLine();
                return;
            }

            //SITEID = "01528";

            Console.WriteLine($"URI:{URI}");
            Console.WriteLine($"SiteID:{SITEID}");
            Console.WriteLine($"Idp:{IDP}");
            Console.WriteLine($"User:{user}");
            Console.WriteLine($"Pass:{pass}");
            Console.WriteLine($"Path:{watchLocation}");

            if (!IsBackFill)
            {

                FileSystemWatcher _fsWatcher = new FileSystemWatcher(watchLocation, "*.json");
                Console.WriteLine("Hello World!");

                Task.Run(() => Program.AuthService.AuthenticateUser(Program.user, Program.pass));

                try
                {
                    _fsWatcher.Created += _fsWatcher_Created;
                    _fsWatcher.EnableRaisingEvents = true;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }
            else
            {
                var b = Program.AuthService.AuthenticateUser(Program.user, Program.pass).Result;

                List<string> lFiles = Directory.GetFiles(watchLocation, "*.json").ToList();

                lFiles.ForEach(file => {
                    if (!File.Exists(file))
                        return;

                    System.Console.WriteLine(file);
                    UnloadReadsRequest urr = new UnloadReadsRequest();

                    string json = string.Empty;
                    using (TextReader reader = File.OpenText(file))
                    {
                        json = reader.ReadToEnd();
                    }

                    if (string.IsNullOrEmpty(json))
                        return;

                    urr = JsonConvert.DeserializeObject<UnloadReadsRequest>(json);

                    //writeMultiFiles(urr);

                    //string[] fileparts = file.Split('_');
                    //Program.SITEID = fileparts[1];
                    Console.WriteLine($"using siteid [{Program.SITEID}]");

                    string api = string.Format(API, Program.SITEID);

                    Services.ApiServiceBase apiService = new Services.ApiServiceBase();
                    apiService.Initialize(Program.ApiBaseUrl);

                    var token = new System.Threading.CancellationTokenSource().Token;

                    UnloadReadsResponse shit = new UnloadReadsResponse();
                    shit.status = 1;
                    
                    shit = apiService.PutAsync<UnloadReadsResponse, UnloadReadsRequest>(api, token, 0, urr).Result;

                    Console.WriteLine("Result: ", shit.ToString());

                    //if(shit?.status == 0)
                    //    File.Delete(file);

                    System.Threading.Thread.Sleep(3000);

                });
            }

            //while (true)
            //{
            //    //System.Diagnostics.Debug.WriteLine(System.DateTime.Now.ToLongTimeString());
            //    System.Threading.Tasks.Task.Delay(5000);
            //}

            // wait - not to end
            //new System.Threading.AutoResetEvent(false).WaitOne();
        }

        private static async void _fsWatcher_Created(object sender, FileSystemEventArgs e)
        {
            if (!File.Exists(e.FullPath))
                return;

            System.Console.WriteLine(e.FullPath);
            UnloadReadsRequest urr = new UnloadReadsRequest();

            string json = string.Empty;
            using (TextReader reader = File.OpenText(e.FullPath))
            {
                json = reader.ReadToEnd();
            }

            if (string.IsNullOrEmpty(json))
                return;

            urr = JsonConvert.DeserializeObject<UnloadReadsRequest>(json);

            //writeMultiFiles(urr);

            string[] fileparts = e.Name.Split('_');
            Program.SITEID = fileparts[1];
            Console.WriteLine($"using siteid [{SITEID}]");

            string api = string.Format(API, SITEID);

            Services.ApiServiceBase apiService = new Services.ApiServiceBase();
            apiService.Initialize(Program.ApiBaseUrl);

            var token = new System.Threading.CancellationTokenSource().Token;
            var shit = await apiService.PutAsync<UnloadReadsResponse, UnloadReadsRequest>(api, token, 0, urr);

            

            File.Delete(e.FullPath);
        }

        static async void writeMultiFiles(UnloadReadsRequest urr)
        {
            if (urr.Readings.Count < 101)
            {
                using (TextWriter tw = File.CreateText($"{SITEID}-{System.Guid.NewGuid()}.json"))
                {
                    await tw.WriteAsync(JsonConvert.SerializeObject(urr));
                }
            }
            else
            {
                int iCount = 0;
                var pUrr = new UnloadReadsRequest();
                pUrr.IsTrimbleReading = urr.IsTrimbleReading;
                pUrr.Synchronous = urr.Synchronous;
                for (int i = 0; i < urr.Readings.Count; i++)
                {
                    pUrr.Readings.Add(urr.Readings[i]);
                    if ((i + 1) == urr.Readings.Count || ++iCount > 100)
                    {
                        using (TextWriter tw = File.CreateText($"{SITEID}-{System.Guid.NewGuid()}.json"))
                        {
                            iCount = 0;
                            await tw.WriteAsync(JsonConvert.SerializeObject(pUrr));
                            await tw.FlushAsync();
                        }
                        pUrr.Readings = new System.Collections.Generic.List<UnloadReadsRequestRead>();
                    }
                }
            }
        }

        static __AuthService _authService = null;

        public static __AuthService AuthService
        {
            get
            {
                if (_authService == null)
                {
                    _authService = new __AuthService();
                    _authService.Initialize(
                        IdpUrl,
                        Common.Strings.IDPClientID,
                        Common.Strings.IDPClientSecret,
                        Common.Strings.IDPClentScope);
                }
                return _authService;
            }

        }

        static Func<string> IdpUrl = () =>
        {
            return IDP;
        };


        static Func<string> ApiBaseUrl = () => {
            return Program.URI;
        };
    }
}
