﻿using System;
using System.Diagnostics.Contracts;

namespace AMRReadingsFileReprocessor.Interfaces
{
    public interface ILogManager
    {
        ILogger GetLog([System.Runtime.CompilerServices.CallerFilePath]string callerFilePath = "");
    }
}
