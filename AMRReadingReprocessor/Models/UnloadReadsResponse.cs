﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMRReadingReprocessor.Models
{
    public class UnloadReadsResponse
    {
        public int status { get; set; } = 1;
    }
}
