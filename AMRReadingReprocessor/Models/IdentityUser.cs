﻿using System;
namespace AMRReadingReprocessor.Models
{
    public class IndentityUser //: NotifyProperyBase
    {
        /// <summary>
        /// Gets or sets the name of the given.
        /// </summary>
        /// <value>The name of the given.</value>
        public string given_name { get; set; }

        /// <summary>
        /// Gets or sets the name of the family.
        /// </summary>
        /// <value>The name of the family.</value>
        public string family_name { get; set; }

        /// <summary>
        /// user ID
        /// </summary>
        public string sub { get; set; }

       public string role { get; set; }

        /// <summary>
        /// Gets or sets the user utility role.
        /// </summary>
        /// <value>The user utility role.</value>
        public IdentityUserUtilityRole[] UserUtilityRole { get; set; } = { };
    }

    public class IdentityUserUtilityRole
    {
        public int UtilityId { get; set; }
    }
}
