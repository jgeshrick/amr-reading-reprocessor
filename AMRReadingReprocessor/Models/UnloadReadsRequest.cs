﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMRReadingReprocessor.Models
{
    /// <summary>
    /// Device types
    /// </summary>
    public enum eDeviceType
    {
        /// <summary>
        /// undefined device type
        /// </summary>
        undefined,

        /// <summary>
        /// SURF MIU
        /// </summary>
        surf,       // SURF

        /// <summary>
        /// SCM/SCM+ MIU
        /// </summary>
        scm,        // SCM/SCM+

        /// <summary>
        /// Advantage MIU
        /// </summary>
        advantage   // Advantage
    };

    public enum eIndustryType
    {
        /// <summary>
        /// undefined industry type
        /// </summary>
        undefined = -1,

        /// <summary>
        /// water
        /// </summary>
        water = 0,

        /// <summary>
        /// gas
        /// </summary>
        gas,

        /// <summary>
        /// electric
        /// </summary>
        electric,
    };


    public class UnloadReadsRequest
    {
        //public string Token { get; set; }
        public bool Synchronous { get; set; } = true;
        public bool IsTrimbleReading { get; set; } = false;
		public string UserEmail { get; set; }
        public string AppBuild { get; set; }
        public string AppVersion { get; set; }
        public string Id { get; set; }
        public string DeviceName { get; set; }
        public string Idiom { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Platform { get; set; }
        public string Version { get; set; }

        public List<UnloadReadsRequestRead> Readings { get; set; } = new List<UnloadReadsRequestRead>();
    }

    public class UnloadReadsRequestRead
    {
        public string OrderKey { get; set; }
        public string DeviceType { get; set; }
        public string IndustryType { get; set; }
        public string MIUID { get; set; }
        public string MeterType { get; set; }
        public string ReadType { get; set; }
        public int? DisplayDials { get; set; }
        public long? Timestamp { get; set; }
        public string ReadingPacket { get; set; }
        public long? ManualReading { get; set; }
        public string SkipCode { get; set; }
        public string CommentCode1 { get; set; }
        public string CommentCode2 { get; set; }
        public string NoteBack { get; set; }
        public int ReentryCount { get; set; } = 0;
        public string ReadCode { get; set; } = null;
        public string ReaderId { get; set; } = string.Empty;
		public string RouteName { get; set; }
		public int? SplitId { get; set; }
        public string PacketFormat { get; set; } = string.Empty;
    }
}
