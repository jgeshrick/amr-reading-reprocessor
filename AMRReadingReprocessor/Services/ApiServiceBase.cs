﻿
using AMRReadingReprocessor.Common;
//using AMRReadingReprocessor.Interfaces;
using AMRReadingReprocessor.Models;
using RestSharp;
using System;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
//using Xamarin.Forms;

namespace AMRReadingReprocessor.Services
{
	public class NTGApiResult
	{
		public bool Success { get; set; } = true;
	}

	public class NFPProcessFileRequest
	{

		/// <summary>
		/// 
		/// </summary>
		public string SiteId { get; set; } = default;

		/// <summary>
		/// 
		/// </summary>
		public string FileType { get; set; } = default;

	}

	public class ApiServiceBase
	{
		//protected ILogger Log = DependencyService.Get<ILogManager>().GetLog();

		Func<string> _baseUrl = null;

		protected const int _20_MINUTES = (60 * 20 * 1000);
		protected const int _10_MINUTES = (60 * 10 * 1000);
		protected const int _2_MINUTES = (60 * 2 * 1000);
		protected const int _1_MINUTE = (60 * 1 * 1000);
		protected const int _30_SECONDS = (30 * 1 * 1000);

		public bool IsInitialized { get; private set; } = false;

		public ApiServiceBase() { }

		public void Initialize(Func<string> baseUrl)
		{
			this._baseUrl = baseUrl;
			var s = ValidateRequiredMembers();
			if (!string.IsNullOrEmpty(s))
			{
				//Log.Error(s);
				throw new Exception(s);
			}
			this.IsInitialized = true;
		}

		string ValidateRequiredMembers()
		{
			if (this._baseUrl == null)
				return "Remote Url Func<> is Required";
			return string.Empty;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="restPath"></param>
		/// <param name="cancellationToken"></param>
		/// <param name="baseUrl"></param>
		/// <param name="timeOut"></param>
		/// <returns></returns>
//		protected async Task<TValue> GetAsync<TValue>(string restPath, CancellationToken cancellationToken, int? timeOut)
//		{
////#if !DEBUG
////			string baseUrl = "http://10.8.10.177:5000/";
////#else
//			string baseUrl = this._baseUrl();
////#endif

//			Log.Info($"GET: BaseURL:[{baseUrl}], Api:[{restPath}]");

//			var authService = DependencyService.Resolve<AuthService>();
//			if (string.IsNullOrEmpty(authService?.AuthToken))
//			{
//				DependencyService.Get<GlobalWatchList>().LastErrorMessage = "No Auth Token";
//				return default(TValue);
//			}

//			TimeSpan ts = new TimeSpan(0, 0, 0, 0, timeOut ?? _2_MINUTES);

//			var request = new RestRequest(restPath, Method.GET)
//			{
//				OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; },
//				JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer()
//			};
			
//			//request.AddHeader("Authorization", $"Bearer c01c5708d4ec84d369fbc39c8a39b841b1416ce7ba5bee0a822afa2f54a2f358");
//			request.AddHeader("Authorization", $"Bearer {authService.AuthToken}");
//			request.Timeout = ts.Milliseconds;

//			RestClient rc = new RestClient(baseUrl);

//			System.Diagnostics.Debug.WriteLine(baseUrl + restPath);

//			try
//			{
//				IRestResponse<TValue> restResponse = await rc.ExecuteAsync<TValue>(request, cancellationToken);

//				if (restResponse.StatusCode == HttpStatusCode.Unauthorized)
//				{
//					var res = await authService.RefreshTokenAsync();
//					if (res == Enumerations.EnHostCommunicationResult.success)
//					{
//						restResponse = await rc.ExecuteAsync<TValue>(request, cancellationToken);
//						if (restResponse.StatusCode == HttpStatusCode.OK)
//						{
//							return restResponse.Data;
//						}
//					}
//				}
//				else if (restResponse.StatusCode == HttpStatusCode.OK)
//				{
//					return restResponse.Data;
//				}

//			}
//			catch (Exception _ex)
//			{
//				Log.Error(_ex.TraceSummary());
//				throw _ex;
//			}
//			return default(TValue);
//		}


	

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TValue"></typeparam>
		/// <typeparam name="TRequestData"></typeparam>
		/// <param name="restPath"></param>
		/// <param name="cancellationToken"></param>
		/// <param name="baseUrl"></param>
		/// <param name="timeOut"></param>
		/// <param name="requestData"></param>
		/// <returns></returns>
		public async Task<TValue> PutAsync<TValue, TRequestData>(string restPath, CancellationToken cancellationToken, int? timeOut, TRequestData requestData = default(TRequestData))
		{
			string baseUrl = this._baseUrl();

			//Log.Info($"PUT: BaseURL:[{baseUrl}], Api:[{restPath}]");

			if (string.IsNullOrEmpty(Program.AuthService?.AuthToken))
			{
				System.Console.WriteLine("No Auth Token");
				return default(TValue);
			}
			TimeSpan ts = new TimeSpan(0, 0, 0, 0, timeOut ?? _2_MINUTES);

			var request = new RestRequest(restPath, Method.PUT)
			{
				OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; },
				RequestFormat = DataFormat.Json
			};

			request.AddJsonBody(requestData);
			request.AddHeader("Authorization", $"Bearer {Program.AuthService.AuthToken}");
			request.Timeout = ts.Milliseconds;

			RestClient rc = new RestClient(baseUrl);

			try
			{
				IRestResponse<TValue> restResponse = await rc.ExecuteAsync<TValue>(request, cancellationToken);
				if (restResponse.StatusCode == HttpStatusCode.Unauthorized)
				{
					var res = await Program.AuthService.RefreshTokenAsync();
					if (res == Enumerations.EnHostCommunicationResult.success)
					{
						restResponse = await rc.ExecuteAsync<TValue>(request, cancellationToken);
						if (restResponse.StatusCode == HttpStatusCode.OK)
						{
							return restResponse.Data;
						}
					}
				}
				else if (restResponse.StatusCode == HttpStatusCode.OK)
				{
					return restResponse.Data;
				}
			}
			catch (Exception _ex)
			{
				Console.WriteLine($"Exception: {_ex.Message}");
				//Log.Error(_ex.TraceSummary());
				throw _ex;
			}
			return default(TValue);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TValue"></typeparam>
		/// <typeparam name="TRequestData"></typeparam>
		/// <param name="restPath"></param>
		/// <param name="cancellationToken"></param>
		/// <param name="baseUrl"></param>
		/// <param name="timeOut"></param>
		/// <param name="sendFile"></param>
		/// <param name="requestData"></param>
		/// <returns></returns>
//		protected async Task<TValue> SendFileAsync<TValue, TRequestData>(string restPath, CancellationToken cancellationToken, int? timeOut, SendFileModel sendFile, TRequestData requestData = default(TRequestData))
//		{
////#if !DEBUG
////			string baseUrl = "http://10.8.10.177:5000/";
////#else
//			string baseUrl = this._baseUrl();
////#endif

//			Log.Info($"SendFileAsync: BaseURL:[{baseUrl}], Api:[{restPath}]");

//			var authService = DependencyService.Resolve<AuthService>();
//			if (string.IsNullOrEmpty(authService?.AuthToken))
//			{
//				DependencyService.Get<GlobalWatchList>().LastErrorMessage = "No Auth Token";
//				return default(TValue);
//			}
//			TimeSpan ts = new TimeSpan(0, 0, 0, 0, timeOut ?? _1_MINUTE);

//			var request = new RestRequest(restPath, Method.POST)
//			{
//				OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; },
//				RequestFormat = DataFormat.Json
//			};

//			request.AddHeader("Authorization", $"Bearer {authService.AuthToken}");
//			//request.AddJsonBody(requestData);
//			//request.AddParameter("application/json", File.ReadAllBytes(@"C:\Temp\upload.pdf"), ParameterType..RequestBody);
//			//request.AddParameter("application/json", requestData, ParameterType.RequestBody);
//			request.AddFile(sendFile.FileName, sendFile.FullPath);
//			request.Timeout = ts.Milliseconds;

//			RestClient rc = new RestClient(baseUrl);

//			try
//			{
//				IRestResponse<TValue> restResponse = await rc.ExecuteAsync<TValue>(request, cancellationToken);
//				if (restResponse.StatusCode == HttpStatusCode.Unauthorized)
//				{
//					var res = await authService.RefreshTokenAsync();
//					if (res == Enumerations.EnHostCommunicationResult.success)
//					{
//						var tcs = new TaskCompletionSource<IRestResponse<TValue>>();

//						restResponse = await rc.ExecuteAsync<TValue>(request, cancellationToken);
//						if (restResponse.StatusCode == HttpStatusCode.OK)
//						{
//							return restResponse.Data;
//						}
//					}
//				}
//				else if (restResponse.StatusCode == HttpStatusCode.OK)
//				{
//					return restResponse.Data;
//				}
//			}
//			catch (Exception _ex)
//			{
//				//Log.Error(_ex.TraceSummary());
//				throw _ex;
//			}
//			return default(TValue);
//		}



		//public async Task<TValue> SignUpPostAsyncBase<TValue, TRequestData>(string restPath, CancellationToken cancellationToken, TRequestData requestData = default(TRequestData) )
		//{
		//	string baseUrl = this._baseUrl();

		//	Log.Info($"POST: BaseURL:[{baseUrl}], Api:[{restPath}]");

		//	var authService = DependencyService.Resolve<AuthService>();
			
		//	TimeSpan ts = new TimeSpan(0, 0, 0, 0, _1_MINUTE);

		//	var request = new RestRequest(restPath, Method.POST)
		//	{
		//		OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; },
		//		RequestFormat = DataFormat.Json
		//	};

		//	request.AddJsonBody(requestData);

		//	string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Strings.CreateUserName + ":" + Strings.CreateUserPassword));
		//	request.AddHeader("Authorization", $"Basic {svcCredentials}");
		//	request.Timeout = ts.Milliseconds;

		//	RestClient rc = new RestClient(baseUrl);

		//	try
		//	{
		//		IRestResponse<TValue> restResponse = await rc.ExecuteAsync<TValue>(request);
				
		//		if (restResponse.StatusCode == HttpStatusCode.OK)
		//		{
		//			return restResponse.Data;
		//		}
		//	}
		//	catch (Exception _ex)
		//	{
		//		Log.Error(_ex.TraceSummary());
		//		throw _ex;
		//	}
		//	return default(TValue);
		//}

		//protected async Task<TValue> GetAnonymousAsync<TValue>(string restPath, CancellationToken cancellationToken, int? timeOut)
		//{
		//	string baseUrl = this._baseUrl();

		//	Log.Info($"GET: BaseURL:[{baseUrl}], Api:[{restPath}]");

		//	TimeSpan ts = new TimeSpan(0, 0, 0, 0, timeOut ?? _2_MINUTES);

		//	var request = new RestRequest(restPath, Method.GET)
		//	{
		//		OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; },
		//		JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer()
		//	};
		//	request.Timeout = ts.Milliseconds;

		//	RestClient rc = new RestClient(baseUrl);

		//	System.Diagnostics.Debug.WriteLine(baseUrl + restPath);

		//	try
		//	{
		//		IRestResponse<TValue> restResponse = await rc.ExecuteAsync<TValue>(request, cancellationToken);

		//		if (restResponse.StatusCode == HttpStatusCode.OK)
		//		{
		//			return restResponse.Data;
		//		}
		//	}
		//	catch (Exception _ex)
		//	{
		//		Log.Error(_ex.TraceSummary());
		//		throw _ex;
		//	}
		//	return default;
		//}

	}
}
