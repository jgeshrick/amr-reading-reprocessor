﻿using System;
using System.IO;
using NLog;
using NLog.Config;
using NLog.Targets;
using AMRReadingsFileReprocessor.Interfaces;

[assembly: Dependency(typeof(AMRReadingsFileReprocessor.Services.NLogManager))]
namespace AMRReadingsFileReprocessor.Services
{
    public class NLogManager : Interfaces.ILogManager
    {
        public NLogManager()
        {
            var config = new LoggingConfiguration();
            
            var consoleTarget = new ConsoleTarget();
            config.AddTarget("console", consoleTarget);

            var fileTarget = new FileTarget();
            fileTarget.FileName = "AMRReadingsFileReprocessor.log";
            config.AddTarget("file", fileTarget);

            var consoleRule = new LoggingRule("*", LogLevel.Trace, consoleTarget);
            config.LoggingRules.Add(consoleRule);

            NLog.LogLevel ll = LogLevel.Warn;

//#if DEBUG
            ll = LogLevel.Trace;
//#endif
            var fileRule = new LoggingRule("*", ll, fileTarget);
            config.LoggingRules.Add(fileRule);
            LogManager.Configuration = config;
        }

        public Interfaces.ILogger GetLog([System.Runtime.CompilerServices.CallerFilePath] string callerFilePath = "")
        {
            string fileName = callerFilePath;

            if (fileName.Contains("/"))
            {
                fileName = fileName.Substring(fileName.LastIndexOf("/", StringComparison.CurrentCultureIgnoreCase) + 1);
            }

            var logger = LogManager.GetLogger(fileName);
            return new NLogLogger(logger);
        }
    }
}