﻿using System;
using System.Collections.Generic;
using IdentityModel.Client;
using static AMRReadingReprocessor.Common.Enumerations;
using AMRReadingReprocessor.Common;
using System.Net.Http;
using System.Threading.Tasks;
using AMRReadingReprocessor.Models;
using System.Threading;
//using AMRReadingReprocessor.Interfaces;


namespace AMRReadingReprocessor.Services
{
    public class AuthService
    {
        //ILogger Log = DependencyService.Get<ILogManager>().GetLog();

        Func<string> _idpUrl;

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string ClientScope { get; set; }

        public string AuthToken { get; set; }

        public string RefreshToken { get; set; }

        /// <summary>
        /// Provide value in seconds.
        /// </summary>
        public int HttpTimeout { get; set; } = 10;

        public AuthService() {
            
        }

        string ValidateRequiredMembers()
        {
            if (this._idpUrl == null)
                return "Remote IDP Func<> is Required";
            if (string.IsNullOrEmpty(this.ClientId.SafeTrim()))
                return "Remote Client ID is Required";
            if (string.IsNullOrEmpty(this.ClientSecret.SafeTrim()))
                return "Remote Client Secret is Required";
            if (string.IsNullOrEmpty(this.ClientScope.SafeTrim()))
                return "Remote Client Scope is Required";
            return string.Empty;
        }

        public bool IsInitialized { get; private set; } = false;

        public void Initialize(
            Func<string> idpUrl,
            string clientId,
            string clientSecret,
            string clientScope,
            int httpTimeout = 30)
        {
            this._idpUrl = idpUrl;
            this.ClientId = clientId;
            this.ClientSecret = clientSecret;
            this.ClientScope = clientScope;
            this.HttpTimeout = httpTimeout;

            var s = ValidateRequiredMembers();
            if (!string.IsNullOrEmpty(s))
            {
                //Log.Error(s);
                throw new Exception(s);
            }

            this.IsInitialized = true;
        }

        //public AuthService(string idpAddress, string clientId, string clientSecret, string clientScope, int httpTimeout = 10)
        //{
        //    this.Initialize(idpAddress, clientId, clientSecret, clientScope, httpTimeout);
        //}

        public IndentityUser IdentityUserFromToken
        {
            get {
                if (string.IsNullOrEmpty(this.AuthToken))
                    return null;

                try
                {
                    var decodedToken = this.AuthToken.DecodeAuthToken();
                    IndentityUser pUser = Newtonsoft.Json.JsonConvert.DeserializeObject<IndentityUser>(decodedToken);
                    return pUser;
                }
                catch (Exception _ex)
                {
                    //Log.Error(_ex.Message, _ex);
                }

                return null;
            }
        }

        public async Task<bool> AuthenticateUser(string username, string password)
        {
            var s = ValidateRequiredMembers();
            if (!string.IsNullOrEmpty(s))
            {
                //Log.Error(s);
                throw new Exception(s);
            }

            bool retVal = false;
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = new TimeSpan(0, 0, 0, this.HttpTimeout, 0);

                    var url = this._idpUrl();

                    var disco = await client.GetDiscoveryDocumentAsync(
                        new DiscoveryDocumentRequest
                        {
                            Address = url,
                            Policy = {
                                RequireHttps = url.ToLower().StartsWith("https:", StringComparison.CurrentCulture) ? true : false,
                                ValidateEndpoints = false,
                                ValidateIssuerName = false
                            }
                        });

                    if (disco.IsError) {
                        //Log.Error(disco.Error);
                        throw new Exception(disco.Error);
                    }

                    TokenResponse tokenResponse = await client.RequestPasswordTokenAsync(new PasswordTokenRequest
                    {
                        Address = disco.TokenEndpoint,
                        ClientId = this.ClientId,
                        ClientSecret = this.ClientSecret,
                        Scope = this.ClientScope,
                        UserName = username,
                        Password = password
                    });

                    if (tokenResponse.IsError)
                    {
                        throw new Exception(tokenResponse.Error);
                    }

                    this.AuthToken = tokenResponse?.AccessToken;
                    this.RefreshToken = tokenResponse?.RefreshToken;

                    retVal = true;
                }
            }
            catch (Exception _ex)
            {
                //Log.Error(_ex.Message);
                throw _ex;
            }
            return retVal;
        }

        public async Task<EnHostCommunicationResult> RefreshTokenAsync()
        {
            var s = ValidateRequiredMembers();
            if (!string.IsNullOrEmpty(s))
            {
                throw new Exception(s);
            }

            EnHostCommunicationResult lResult = EnHostCommunicationResult.communicationError;

            try
            {
                if (string.IsNullOrEmpty(this.RefreshToken.SafeTrim()))
                {
                    throw new Exception("Invalid Refresh Token");
                }

                using (var client = new HttpClient())
                {
                    client.Timeout = new TimeSpan(0, 0, 0, this.HttpTimeout, 0);

                    var url = this._idpUrl();

                    var disco = await client.GetDiscoveryDocumentAsync(
                        new DiscoveryDocumentRequest
                        {
                            Address = this._idpUrl(),
                            Policy = {
                                RequireHttps = url.ToLower().StartsWith("https:", StringComparison.CurrentCulture) ? true : false
                            }
                        });

                    if (disco.IsError)
                    {
                        //DependencyService.Get<GlobalWatchList>().LastErrorMessage = disco.Error;
                        //Log.Warn(disco.Error);
                        throw new Exception(disco.Error);
                    }

                    var tokenResponse = await client.RequestRefreshTokenAsync(
                        new RefreshTokenRequest
                        {
                            Address = disco.TokenEndpoint,
                            ClientId = this.ClientId,
                            ClientSecret = this.ClientSecret,
                            RefreshToken = this.RefreshToken
                        });

                    if (!tokenResponse.IsError)
                    {
                        AuthToken = tokenResponse?.AccessToken;
                        RefreshToken = tokenResponse.RefreshToken;
                        lResult = EnHostCommunicationResult.success;
                    }
                    else if (tokenResponse.Error == "invalid_grant")
                    {
                        //DependencyService.Get<GlobalWatchList>().LastErrorMessage = "Invalid Credentials";
                        lResult = EnHostCommunicationResult.invalidCredentials;
                    }
                }
            }
            catch (Exception _ex)
            {
                //Log.Error(_ex.Message);
                throw _ex;
            }
            return lResult;
        }
    }
}
