﻿using AMRReadingReprocessor.Models;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

namespace AMRReadingReprocessor.Services
{
	public class ApiService : ApiServiceBase
    {
		public ApiService(){ }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="aCancelToken"></param>
		/// <returns></returns>
		//public async Task<List<UtilitySiteIDDTO>> GetUtilisByUser(int userId, CancellationToken aCancelToken)
		//{
		//	string api = string.Format(__NCS.GetUtilitiesByUserId, userId);
		//	var response = await GetAsync<List<UtilitySiteIDDTO>>(api, aCancelToken, _30_SECONDS);
		//	return response;
		//}

		///// <summary>
		///// 
		///// </summary>
		///// <param name="sfm"></param>
		///// <param name="aCancelToken"></param>
		///// <returns></returns>
		//public async Task<List<NFPFormat>> RetrievePresetFormats(DateTime loadDate, CancellationToken aCancelToken)
		//{
		//	string api = string.Format(__FPS.PresetFormatsV1, loadDate.ToString("yyyy-MM-ddTHH:mm:ss"));
		//	var response = await GetAsync<List<NFPFormat>>(api, aCancelToken, _30_SECONDS);
		//	return response;
		//}

		///// <summary>
		///// 
		///// </summary>
		///// <param name="sfm"></param>
		///// <param name="aCancelToken"></param>
		///// <returns></returns>
		//public async Task<bool> SendTransactionFile(SendFileModel sfm, CancellationToken aCancelToken)
		//{
  //          string api = string.Format(__FPS.UploadDataFileV1, "Programming", CrossDeviceInfo.Current.Id);
  //          var response = await SendFileAsync<bool, NFPProcessFileRequest>(api, aCancelToken, _30_SECONDS, sfm);
  //          return response;
  //      }

		///// <summary>
		///// 
		///// </summary>
		///// <param name="sfm"></param>
		///// <param name="aCancelToken"></param>
		///// <returns></returns>
  //      public async Task<bool> SendLogFiles(SendFileModel sfm, CancellationToken aCancelToken)
  //      {
  //          string api = string.Format(__FPS.UploadDataFileV1, "Logs", CrossDeviceInfo.Current.Id);
  //          var response = await SendFileAsync<bool, NFPProcessFileRequest>(api, aCancelToken,  _30_SECONDS, sfm);
  //          return response;
  //      }

		//public async Task<string> GetAppVersions(CancellationToken aCancelToken)
		//{
		//	string api = __NCS.GetAppVersions;
		//	var response = await GetAsync<string>(api, aCancelToken, _30_SECONDS);
		//	return response;
		//}

		//public async Task<CreateMobileApplicationUserResponse> CreateMobileApplicationUser(CreateMobileApplicationUserRequest request, CancellationToken aCancelToken)
		//{
		//	string api = SharedServices.IdentityService.CreateMobileApplicationUser;
		//	var response = await SignUpPostAsyncBase<CreateMobileApplicationUserResponse, CreateMobileApplicationUserRequest>(api, aCancelToken, request);
		//	return response;
		//}

		//public async Task<string> GetTrimbleCabFile(string version, CancellationToken aCancelToken)
		//{
		//	string api = string.Format(API_TRIMBLE_CAB, version);
		//	var response = await GetAsync<string>(api, aCancelToken, this._baseUrl(), _20_MINUTES);
		//	return response;
		//}

		//public async Task<Utility> GetUtilitySiteID(CancellationToken aCancelToken)
		//{
		//	string api = string.Format(API_UTILITIE, SiteID);
		//	var response = await GetAsync<Utility>(api, aCancelToken, this._baseUrl(), _2_MINUTES);

		//	return response;
		//}


		//public async Task<List<RouteAssignment>> GetRoutesByDevice(string machineId, string crewId, CancellationToken aCancelToken)
		//{
		//	string api = string.Format(API_RoutesByDevice, machineId, SavedUtility.UtilityId, SiteID, crewId);
		//	var response = await GetAsync<List<RouteAssignment>>(api, aCancelToken, this._baseUrl(), _2_MINUTES);
		//	return response;
		//}

		//public async Task<HandheldAggregateSettings> GetDeviceSettings(string machineId, bool forceOverride, CancellationToken aCancelToken)
		//{
		//	//string api = string.Format(API_DeviceSettings, machineId, SavedUtility.UtilityId, forceOverride);
		//	string api = string.Format(API_DeviceSettings, machineId, SavedUtility.UtilityId, true);
		//	var response = await GetAsync<HandheldAggregateSettings>(api, aCancelToken, this._baseUrl(), _2_MINUTES);
		//	return response;
		//}



	}
}
